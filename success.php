<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Вершинин");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Вершинин");
?>
<main class="content">
  <div class="intro-page">
    <div class="container">
      <div class="intro-page__img bg-img-cover" style="background-image: url(/images/project/bg.jpg);"></div>
    </div>
  </div>

  <div class="container">
    <h1 class="tb-title title-1 m-b-30 m-lg-b-60 font-w-bold">Спасибо!</h1>
    <div class="title-6 ">
      Сообщение успешно отправлено. Наш менеджер свяжется с Вами в ближайшее время.
    </div>
    <div class="m-y-60">
      <a href="/" class="btn btn-default">Перейти на главную</a>
    </div>
  </div>
</main>
</div>
<!--/wrapper-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>