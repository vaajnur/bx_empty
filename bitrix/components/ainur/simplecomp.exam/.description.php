<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MY_EXAM_DETAIL_NAME"),
	"DESCRIPTION" => GetMessage("MY_EXAM_DETAIL_DESC"),
	"ICON" => "/images/news_detail.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "simplecomp.exam",
		"SORT" => 2000,
		"NAME" => GetMessage("MY_EXAM"),
		"CHILD" => array(
			"ID" => "simplecomp.exam",
			"NAME" => GetMessage("MY_COMPONENT_NAME"),
			"SORT" => 10,
		)
	),
);

?>