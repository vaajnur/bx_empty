<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1><?=$arParams['TITLE'] . ' ' .$arResult['COUNT'];?> </h1>
<? if(!empty($arResult['ITEMS'])): ?>
	<? foreach($arResult['ITEMS'] as $k => $item): ?>

	<div class="news-detail">
			<p> <b><?=$item["NAME"]?></b> <span><?=$item["ACTIVE_FROM"]?></span> (<?=implode(', ' , array_column($item["SECTIONS"], 'NAME'))?>)</p>
			<ul>
				<? foreach($item['ELEMENTS'] as $k1 => $elem): ?>
					<li><?=$elem['NAME'];?> - <?=$elem['PRICE']['PRICE'];?> - <?=$elem['PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?> - <?=$elem['PROPERTIES']['CML2_ARTICLE']['VALUE'];?> </li>
				<? endforeach;?>
			</ul>
		<div style="clear:both"></div>
		<br />


	</div>

	<? endforeach ;?>
<? endif ;?>