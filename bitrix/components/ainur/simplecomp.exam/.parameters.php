<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"TITLE" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"CATALOG_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => 'ID инфоблока с каталогом товаров',
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"NEWS_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => 'ID инфоблока с новостями',
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"NEWS_LINK" => Array(
			"PARENT" => "BASE",
			"NAME" => 'Код пользовательского свойства разделов каталога, в котором хранится привязка к новостям',
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"SET_TITLE" => Array(),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>
