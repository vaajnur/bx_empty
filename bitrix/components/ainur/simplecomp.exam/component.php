<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($this->StartResultCache(false, array($USER->GetGroups())))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	if($arParams["NEWS_ID"] == '' || $arParams["CATALOG_ID"] == '' || $arParams["NEWS_LINK"] == ''){
		$this->AbortResultCache();
		ShowError('Не заданы параметры компонента');
		return;
	}

	$arSelect = Array(
		"ID",
		"NAME",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_PICTURE",
		"ACTIVE_FROM",
		"LIST_PAGE_URL",
	);

	$arFilter = array(
		"IBLOCK_ID" => $arParams["NEWS_ID"],
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		// "ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		// "CHECK_PERMISSIONS" => "Y",
		// "SHOW_HISTORY" => "N",
	);

	$rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

	$all_sections = [];
	while($obElement = $rsElement->GetNextElement())
	{
		$EL = $obElement->GetFields();
		$arResult['ITEMS'][$EL['ID']] = $EL;
		if(isset($EL["PREVIEW_PICTURE"]))
			$arResult['ITEMS'][$EL['ID']]["PREVIEW_PICTURE"] = CFile::GetFileArray($EL["PREVIEW_PICTURE"]);
		if(isset($EL["DETAIL_PICTURE"]))
			$arResult['ITEMS'][$EL['ID']]["DETAIL_PICTURE"] = CFile::GetFileArray($EL["DETAIL_PICTURE"]);

		// SECTIONS
	  $IBLOCK_ID = $arParams["CATALOG_ID"];
	  $UF = $arParams["NEWS_LINK"];
	  $ID = $EL['ID'];
	  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', '='.$UF=> [$ID]);
	  $db_list = CIBlockSection::GetList(Array($by=>''), $arFilter, true);
	  
	  $arSections = [];
	  while($row = $db_list->GetNext()){
	    // pr($row['NAME']());
	    $arResult['ITEMS'][$EL['ID']]['SECTIONS'][] = $row;
	    $arSections[] = $row['ID'];
	  }
	  $all_sections = array_merge($all_sections, $arSections);
	  // pr($arSections);

	  // ELEMENTS
	  $arSelect = Array("ID", "NAME", "IBLOCK_ID", '*');
		$arFilter = Array("IBLOCK_ID"=>IntVal($arParams["CATALOG_ID"]), "ACTIVE"=>"Y", 'SECTION_ID' => $arSections, "INCLUDE_SUBSECTIONS" => "Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
		 	$arFields = $ob->GetFields();
			$arResult['ITEMS'][$EL['ID']]['ELEMENTS'][$arFields['ID']] = $arFields;
			$arResult['ITEMS'][$EL['ID']]['ELEMENTS'][$arFields['ID']]['PRICE'] = CCatalogProduct::GetOptimalPrice($arFields['ID'], 1)['PRICE'];
			// pr($arResult['ITEMS'][$EL['ID']]['ELEMENTS'][$arFields['ID']]['PRICE']);
		      if($ex = $APPLICATION->GetException()){
			  	echo $ex->GetString();
			  }
		 	// pr($arFields['NAME']);
		 	$arProps = $ob->GetProperties();
			$arResult['ITEMS'][$EL['ID']]['ELEMENTS'][$arFields['ID']]['PROPERTIES'] = $arProps;
		}

	}

	// ALL SECTIONS ELEMENTS COUNT
		  $arSelect = Array("ID", "NAME", "IBLOCK_ID");
		  $all_sections = array_unique($all_sections);
		  // pr($all_sections);
		$arFilter = Array("IBLOCK_ID"=>IntVal($arParams["CATALOG_ID"]), "ACTIVE"=>"Y", 'SECTION_ID' => $all_sections, "INCLUDE_SUBSECTIONS" => "Y");
		$res1 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arResult['COUNT'] = $res1->SelectedRowsCount();
		// var_dump($res1->SelectedRowsCount());

		$this->IncludeComponentTemplate();
}


?>
