<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=<?=SITE_CHARSET?>"/>
	<link href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/template_styles.css")?>" type="text/css" rel="stylesheet" />
	<link rel="canonical" href="<? $APPLICATION->ShowProperty('canonical', 'test')?>"/>
	<?
/*	\Bitrix\Main\UI\Extension::load([
		'sidepanel',
		'intranet.sidepanel.bindings',
		'intranet.sidepanel.external',
		'socialnetwork.slider',
	]);*/
	?>
	<?$APPLICATION->ShowCSS(true, true);?>
	<?$APPLICATION->ShowHeadStrings();?>
	<?$APPLICATION->ShowHeadScripts();?>
<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body class="<?=$APPLICATION->ShowProperty("BodyClass");?>">
	  <?=$APPLICATION->ShowPanel()?>

	  <?$APPLICATION->IncludeComponent("bitrix:menu",".default",Array(
        "ROOT_MENU_TYPE" => "top", 
        "MAX_LEVEL" => "1", 
        "CHILD_MENU_TYPE" => "top", 
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "Y",
        "MENU_CACHE_TYPE" => "N", 
        "MENU_CACHE_TIME" => "3600", 
        "MENU_CACHE_USE_GROUPS" => "Y", 
        "MENU_CACHE_GET_VARS" => "" 
    )
);?>