<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arParams['IBLOCK_ID_CANON'] != false){
	$res = ciblockelement::getlist([], ['IBLOCK_ID' => $arParams['IBLOCK_ID_CANON'] ,  '=PROPERTY_NEWS' => $arResult['ID'] , false, ['nTopCount' => 1] , ['ID', 'NAME'] ]);
	if($ob = $res->getnext()){
		$APPLICATION->SetPageProperty('canonical', $ob['NAME']);
	}
}

