<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization;
use \Bitrix\Main\Page\Asset;

$Asset = Asset::getInstance();
?>
<!-- Include Libs & Plugins
  ============================================ -->
  <!-- Placed at the end of the document so the pages load faster -->
  <?
$Asset->addString('<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">');
$Asset->addCss(SITE_TEMPLATE_PATH."/vendor/bootstrap/css/bootstrap-custom.min.css");
$Asset->addCss(SITE_TEMPLATE_PATH."/vendor/slick/slick.css");
$Asset->addCss(SITE_TEMPLATE_PATH."/vendor/slick/slick-theme.css");
$Asset->addCss(SITE_TEMPLATE_PATH."/vendor/fancybox/jquery.fancybox.min.css");
$Asset->addCss(SITE_TEMPLATE_PATH."/css/helper-classes.min.css");
$Asset->addCss(SITE_TEMPLATE_PATH."/css/main.css");

 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/jquery/jquery-3.3.1.min.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/bootstrap/js/bootstrap.min.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/slick/slick.min.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/mask/jquery.mask.min.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/object-fit/ofi.min.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/fancybox/jquery.fancybox.min.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/smoothscrollbar/smooth-scrollbar.js");
 $Asset->addString('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>', false, AFTER_CSS);
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/yandex.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/js/main.js");
?>
<!DOCTYPE html>
<html lang="ru">

<head>

  <!-- Meta tags -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" id="sibling">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

   <!-- Favicon -->
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH;?>/images/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH;?>/images/favicon-32x32.png" sizes="32x32">

  <!-- Title -->
  <title><?$APPLICATION->ShowTitle()?></title>
<? $APPLICATION->ShowHead();?>
  <!-- CSS -->




  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<?php include 'svg.php'; ?>

<body class="home">
  <?=$APPLICATION->ShowPanel()?>
  <div class="wrapper">

    <!--header-->
    <div id="top">
      <!-- Page header -->
      <header class="page-header">
        <!-- ph-top -->
        <div class="ph-top ">
          <div class="container">
            <!--contact-->
            <div class="d-flex align-items-center justify-content-between">
              <!-- logo -->
              <a href="/" class="d-flex align-items-center">
                <div class="ph-logo flex-static">
                  <img class="h-100 w-100" src="<?=SITE_TEMPLATE_PATH;?>/images/icons/logo.png" alt="">
                </div>
              </a>
              <div class="d-flex align-items-center justify-content-between">
                <!-- Main navbar -->
                <div class="main-navbar">
                  <nav class="navbar">
                    <div class="navbar-collapse collapse" id="main-navbar-block" aria-expanded="false">

    <?
    $APPLICATION->IncludeComponent("bitrix:menu","redpromo",Array(
        "ROOT_MENU_TYPE" => "top", 
        "MAX_LEVEL" => "1", 
        "CHILD_MENU_TYPE" => "top", 
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "Y",
        "MENU_CACHE_TYPE" => "N", 
        "MENU_CACHE_TIME" => "3600", 
        "MENU_CACHE_USE_GROUPS" => "Y", 
        "MENU_CACHE_GET_VARS" => "" 
    )
);
?>

                      




                      <div class="header__call-phone hidden-md-up">
                        <a href="tel: +7 (900) 000 00 00" class="link">+7 (900) 000 00
                          00</a>
                      </div>
                      <div class="header__call hidden-xl-up">
                        <a href="#modal-contact" data-toggle="modal" class="link header__call-text">Заказать
                          звонок</a>
                        <div class="small-icon">
                          <div class="si-img">
                            <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                              <use xlink:href="#i-phone"></use>
                            </svg>
                          </div>
                        </div>
                        <div class="header__call-phone hidden-md-down">
                          <a href="tel: +7 (900) 000 00 00" class="link">+7 (900) 000 00
                            00</a>
                        </div>
                      </div>
                    </div>

                  </nav>
                </div>
              </div>
              <!-- /logo -->



              <div class=" d-flex align-items-center justify-content-end flex-column flex-md-row">
                <div class="navbar hidden-xl-up ">
                  <div class="nav-tg-bg">
                    <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#main-navbar-block" aria-expanded="false">
                      <svg class="svg-icon closed" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#i-menu"></use>
                      </svg>
                      <svg class="svg-icon open" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#i-close"></use>
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /ph-top -->

      </header><!-- /Page header -->

    </div><!-- /header -->


