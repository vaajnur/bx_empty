<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="apartment p-y">
    <div class="container">
        <div class="s-title">
            <div class="s-subtitle m-b-20">планировки</div>
            <div class="title-1">Наши <b>квартиры</b></div>
        </div>
        <div class="f-row m-t-40 m-n-b-80">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

if($arItem["PROPERTIES"]["FILE"]['VALUE'] != false){
	$file_for_download  = cfile::GetPath($arItem["PROPERTIES"]["FILE"]['VALUE']);
	// pr($file_for_download);
}
	?>


<div class="f-col-12 f-col-md-6 f-col-xl-4 m-b-50"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    <a href="#" data-toggle="modal">
        <div class="card__apartment">
            <div class="card__apartment-img bg-img-contain"
                style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>);">
            </div>
            <div class="title-2 m-b-25"><?echo $arItem["NAME"]?></div>
            <div class="d-flex">
                <div class="card__apartment-info">
                    <div class="card__apartment-info-title m-b-5">Площадь</div>
                    <div class="card__apartment-number"><?=$arItem["PROPERTIES"]["SQUARE"]['VALUE'];?> м²</div>
                </div>
                <div class="card__apartment-info">
                    <div class="card__apartment-info-title m-b-5">Комнат</div>
                    <div class="card__apartment-number"><?=$arItem["PROPERTIES"]["ROOMS"]['VALUE'];?></div>
                </div>
            </div>
            <hr>
            <div class="link-icon">
                <div class="small-icon">
                    <div class="si-text font-w-semibold m-r-5">
                        <a href="<?=$file_for_download;?>"><span>Скачать планировку</span></a>
                    </div>
                    <div class="si-img">
                        <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="#i-arrow-right"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>



<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
        </div>
    </div>
</section>