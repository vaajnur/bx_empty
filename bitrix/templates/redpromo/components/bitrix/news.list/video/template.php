<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!--pr-slider-->
<div class="pr-slider m-t-60">
    <div class="slick-slider-alt slick-visibility">


<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

if($arItem["PROPERTIES"]["FILE"]['VALUE'] != false){
	$file_for_download  = cfile::GetPath($arItem["PROPERTIES"]["FILE"]['VALUE']);
	// pr($file_for_download);
}
	?>





        <a  id="<?=$this->GetEditAreaId($arItem['ID']);?>" href="<?=$arItem["PROPERTIES"]["YOUTUBE"]['VALUE'];?>" class="pr-item ss-item "
            data-fancybox="big-slider">
            <div class="progress-slider__img object-fit ">
                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
            </div>
            <div class="title-4 m-t-20"><?echo $arItem["NAME"]?></div>
        </a>


<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

    </div>

    <div class="pr-controls" style="display: none">
        <div class="slick-switches  d-flex align-items-center justify-content-between">
            <div class="ss-switch prev m-r-20">
                <div class="svg-switch">
                    <svg class="svg-icon svg-icon__prev" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#i-arrow-left"></use>
                    </svg>
                </div>
            </div>
            <div class="slick-switches dots ">
                <div class="ss-dots d-flex"></div>
            </div>
            <div class="ss-switch next">
                <div class="svg-switch">
                    <svg class="svg-icon svg-icon__next" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#i-arrow-right"></use>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
