<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!--pr-slider-->
<div class="slick-slider slider-visibility">
    


<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	?>

    <div  id="<?=$this->GetEditAreaId($arItem['ID']);?>"  class="ms-slide">
        <div
            class="min-height-fix d-flex flex-column w-100 justify-content-start justify-content-md-center">
            <div class="container flex-grow flex-md-static w-100">
                <div class="mss-content">
                    <div class="mssc-subtitle s-subtitle m-b-20"><?=$arItem["PROPERTIES"]["SUBTITLE"]['VALUE'];?></div>
                    <h1 class="mssc-title title-slide m-y-0"><?=html_entity_decode($arItem["PROPERTIES"]["TITLE_SLIDE"]['VALUE']['TEXT']);?></h1>
                    <div class="mssc-text title m-t-25 m-b-40"><?=$arItem["PROPERTIES"]["SUBTITLE_BOTTOM"]['VALUE'];?></div>

                    <div class="m-t-auto">
                        <div class="d-flex flex-wrap">
                            <a href="/" class="mssc-btn btn btn-default"><?=$arItem["PROPERTIES"]["BTN"]['VALUE'];?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
