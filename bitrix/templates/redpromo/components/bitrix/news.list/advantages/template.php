<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="advantages p-y">
    <div class="container">
        <div class="s-title">
            <div class="s-subtitle m-b-20">преимущества</div>
            <div class="title-1">ЖК «Вершина» <b>в деталях</b></div>
        </div>

        <div class="f-row p-t-80">

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $K=> $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));


	?>

<?

switch ($K) {
    case '0':
    ?>
            <div  id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="f-col-12 f-col-md-6 f-col-lg-5 f-col-xl-7">
                <div class="advantages__img object-fit rellax" data-rellax-speed="4" data-rellax-percentage="0.5">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
                </div>
            </div>
            <div  class="f-col-12 f-col-md-4 f-col-xl-3">
                <div class="advantages__info-text">
                    <div class="title-5 advantages__title"><?echo $arItem["NAME"]?></div>
                    <div class="title-4 advantages__text"><?=$arItem["PROPERTIES"]["SUBTITLE"]['VALUE']['TEXT'];?></div>
                </div>
            </div>
      <?
        break;
    
    case '1':
        ?>

            <div  id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="f-col-12 f-col-md-4 f-col-xl-3">
                <div class="advantages__info-text">
                    <div class="title-5 advantages__title"><?echo $arItem["NAME"]?></div>
                    <div class="title-4 advantages__text"><?=$arItem["PROPERTIES"]["SUBTITLE"]['VALUE']['TEXT'];?></div>
                </div>
            </div>
            <div class="f-col-12 f-col-md-4 f-col-xl-5">
                <div class="advantages__img_text rellax" data-rellax-speed="4" data-rellax-percentage="0.5">
                    <div class="advantages__img2 object-fit">
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
                    </div>
                    <div class="bg-img-contain text-element">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/images/icons/text-2.svg" alt="">
                    </div>
                </div>
            </div>
        <?
        break;
    
    case '2':
        ?>
            
            <div  id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="f-col-12 f-col-md-4 f-col-xl-3">
                <div class="advantages__info-text">
                    <div class="title-5 advantages__title"><?echo $arItem["NAME"]?></div>
                    <div class="title-4 advantages__text"><?=$arItem["PROPERTIES"]["SUBTITLE"]['VALUE']['TEXT'];?></div>
                </div>
            </div>
            <div class="f-col-12">
                <div class="advantages__img_letter">
                    <div class="parallax">
                        <div class="parallax-bg z-n-top " id="parallax-block-3">
                            <div class="parallax-item" data-depth="0.4" data-was-processed="true">
                                <div class="bg-img-contain letter-element">
                                    <img src="<?=SITE_TEMPLATE_PATH;?>/images/icons/Sh.svg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="advantages__img3 object-fit rellax" data-rellax-speed="4"
                        data-rellax-percentage="0.5">
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
                    </div>
                </div>
            </div>
        <?
        break;
    
    default:
        ?>

        <?
        break;
}

?>




<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

        </div>
    </div>
</section>