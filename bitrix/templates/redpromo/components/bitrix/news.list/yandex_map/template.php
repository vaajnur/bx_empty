<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

$map_props = $arItem['PROPERTIES'];
	?>



    <section  id="<?=$this->GetEditAreaId($arItem['ID']);?>"  class="map" id="mapLoc">
        <div id="map" class="w-100"></div>
    </section>
        <script>
            window.mapYandex = {'left':'<?=$map_props['COORDINATES_X']['VALUE']?>', 'right':'<?=$map_props['COORDINATES_Y']['VALUE']?>', 'dot_x':'<?=$map_props['DOT_X']['VALUE']?>', 'dot_y':'<?=$map_props['DOT_Y']['VALUE']?>', 'site_temp_path': '<?=SITE_TEMPLATE_PATH;?>'};
        </script>



<?endforeach;?>