ymaps.ready(function () {
    console.log(window.mapYandex)
    if(window.mapYandex !== undefined){
    var left =  mapYandex.left, 
        right =  mapYandex.right, 
        dot_x = mapYandex.dot_x, 
        dot_y =   mapYandex.dot_y,
        image_path = mapYandex.site_temp_path + '/images/icons/map.svg'
    }else{
        var left =  "55.751574", 
        right =  "37.573856", 
        dot_x =  "-5", 
        dot_y =  "-38",
        image_path = '/images/icons/map.svg'
    }
    var myMap = new ymaps.Map('map', {
            center: [left, right],
            zoom: 13,
            controls: [],
        }, {
            searchControlProvider: 'yandex#search'
        }),


        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: image_path,
            // Размеры метки.
            iconImageSize: [85, 97],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [dot_x, dot_y]
        })

    myMap.geoObjects
        .add(myPlacemark)

        //отключаем зум колёсиком мышки
myMap.behaviors.disable('scrollZoom');
 
//на мобильных устройствах... (проверяем по userAgent браузера)
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    //... отключаем перетаскивание карты
    myMap.behaviors.disable('drag');
}
});


