<div id="bottom" class="footer">
    <div class="contact-info bg-clr-dark">
        <div class="container ">
            <div class="f-row justify-content-between p-y-50">
                <div class="f-col-12 f-col-xl-4">
                    <div class="title contact-info-title m-b-30">Офис продаж</div>
                </div>
                <div class="f-col-12 f-col-xl-3">
                    <div class="m-b-30">
                        <div class="c-item">
                            <div class="small-icon ">
                                <div class="si-img m-r-15">
                                    <div class="si-circle">
                                        <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#i-phone"></use>
                                        </svg>
                                    </div>
                                </div>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include", 
                                        ".default", 
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/inc/phone.php",
                                            "COMPONENT_TEMPLATE" => ".default"
                                        ),
                                        false
                                    );?>
                            </div>
                        </div>
                    </div>
                    <div class="m-b-30">
                        <div class="c-item">
                            <div class="small-icon ">
                                <div class="si-img m-r-15">
                                    <div class="si-circle">
                                        <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#i-mail"></use>
                                        </svg>
                                    </div>
                                </div>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include", 
                                        ".default", 
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/inc/mail.php",
                                            "COMPONENT_TEMPLATE" => ".default"
                                        ),
                                        false
                                    );?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="f-col-12 f-col-xl-4 m-n-b-30">
                    <div class="m-b-30">
                        <div class="c-item">
                            <div class="small-icon ">
                                <div class="si-img m-r-15">
                                    <div class="si-circle">
                                        <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#i-pin"></use>
                                        </svg>
                                    </div>
                                </div>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include", 
                                        ".default", 
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/inc/address.php",
                                            "COMPONENT_TEMPLATE" => ".default"
                                        ),
                                        false
                                    );?>
                            </div>
                        </div>
                    </div>
                    <div class="m-b-30">
                        <div class="c-item">
                            <div class="small-icon ">
                                <div class="si-img m-r-15">
                                    <div class="si-circle">
                                        <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#i-clock"></use>
                                        </svg>
                                    </div>
                                </div>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include", 
                                        ".default", 
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/inc/time.php",
                                            "COMPONENT_TEMPLATE" => ".default"
                                        ),
                                        false
                                    );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Page footer -->
<footer class="page-footer">
    <div class="container">
        <div class="d-sm-flex justify-content-sm-between">
            <div class="pf-item p-y-15 p-sm-y-20 m-sm-r-15">2021©site.ru</div>
            <div class="pf-item p-b-15 p-sm-y-20 flex-static"><a href="https://red-promo.ru" target="_blank"
                    class="link link-footer">Создание сайта</a> — Red Promo</div>
        </div>
    </div>
</footer>

</div>

<!-- Modal contact -->
<div class="modal fade modal-contact" id="modal-contact" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <div class="small-icon">
                        <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="#i-modal-close"></use>
                        </svg>
                    </div>
                </button>
                <div class="title-1 modal-title text-center" id="contactLabel">Оставьте заявку<br> на обратный звонок
                </div>
            </div>
            <div class="modal-body">
                <form class="no-labels" action="" method="post" enctype="">
                    <div class="form-group">
                        <label class="control-label" for="name-id">Ваше имя</label>
                        <input type="text" name="name" id="name-id" class="form-control" placeholder="Ваше имя">
                    </div>
                    <div class="form-group phone-mask-label">
                        <label class="control-label" for="phone-id">Ваш телефон*</label>
                        <div class="position-relative">
                            <input type="text" name="phone" id="phone-id" class="form-control phone-mask"
                                placeholder="+7 (   )" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="message-id">Ваше сообщение</label>
                        <textarea name="message" id="message-id" class="form-control"
                            placeholder="Ваше сообщение"></textarea>
                    </div>
                    <div class="form-group m-b-30">
                        <div class="modal-note text-center">Нажимая «Отправить», Вы принимаете условия <a
                                href="#">политики конфиденциальности</a></div>
                    </div>
                    <div class="text-center modal-submit">
                        <button type="submit" class="btn btn-default p-x-40" name="send-form" value="1"><span>Отправить</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?
use \Bitrix\Main\Page\Asset;
$Asset = Asset::getInstance();
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/parallax/parallax.js");
 $Asset->addJs(SITE_TEMPLATE_PATH."/vendor/rellax.min.js");
?>
<script>
// Accepts any class name
var rellax = new Rellax('.rellax');
</script>

<script>
function setImage(obj) {
    $(obj).parents('.card__apartment').find('.card3D').toggleClass('hidden');
    $(obj).parents('.card__apartment').find('.card2D').toggleClass('hidden');
}
</script>
</body>

</html>