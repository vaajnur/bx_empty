(function ($) {

  'use strict';

  var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
      if (!uniqueId) {
        uniqueId = "Don't call this twice without a uniqueId";
      }
      if (timers[uniqueId]) {
        clearTimeout(timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout(callback, ms);
    };
  })();


  // Footer
  function footer() {
    if ($("#bottom").length) {
      var height = Math.round(document.getElementById('bottom').offsetHeight);
      $(".wrapper .content").css('padding-bottom', height);
      $("#bottom").css('margin-top', -height);
    }
  };

  // Footer auto on resize
  function footerAuto() {
    $(window).on('resize', function () {
      waitForFinalEvent(function () {
        footer();
      }, 100, "footer");
    });
  };

  // Mask
  function mask() {
    $('.phone-mask').mask('+7 (000) 000-00-00', {
      placeholder: "+7 (___) ___-__-__"
    });
  };

  // Main navbar
  function mainNavbar() {
    $('.main-navbar .dropdown > .dropdown-toggle').on('click', function (e) {
      $(".dropdown-menu > li", $(this).closest(".dropdown").siblings()).removeClass('open');
    });
    $('.main-navbar .dropdown-l1 > a').on('click', function (e) {
      e.preventDefault();
      $(this).closest(".dropdown").siblings().removeClass('open');
      $(this).closest('li').siblings().removeClass('open');
      if ($(this).closest('li').hasClass('open')) {
        $(this).closest('li').removeClass('open');
      } else {
        $(this).closest('li').addClass('open');
      }
    });
    $('.main-navbar .dropdown.show-more > a').on('click', function (e) {
      e.preventDefault();
      $(this).parent().toggleClass('open');
    });
    $('body').on('click', function (e) {
      if (!$('.main-navbar .navbar-nav').is(e.target)
        && $('.main-navbar .navbar-nav').has(e.target).length === 0
        && $('.main-navbar .navbar-nav .open').has(e.target).length === 0
      ) {
        $('.main-navbar .dropdown.show-more').removeClass('open');
        $('.main-navbar .dropdown-l1').removeClass('open');
      }
    });
  }

  // Navbar collapse in header
  function navbarCollapse() {
    $('#top').on('show.bs.collapse', '.collapse', function () {
      $('#top').find('.collapse.in').collapse('hide');
    });
  };

  // Page header navbar
  function pageHeaderNavbar() {
    $('.page-header .ph-navbar .dropdown > .dropdown-toggle').on('click', function (e) {
      $(".dropdown-menu > li", $(this).closest(".dropdown").siblings()).removeClass('open');
    });
    $('.page-header .ph-navbar .dropdown-l1 > a').on('click', function (e) {
      e.preventDefault();
      $(this).closest(".dropdown").siblings().removeClass('open');
      $(this).closest('li').siblings().removeClass('open');
      if ($(this).closest('li').hasClass('open')) {
        $(this).closest('li').removeClass('open');
      } else {
        $(this).closest('li').addClass('open');
      }
    });
    $('.page-header .ph-navbar .dropdown.show-more > a').on('click', function (e) {
      e.preventDefault();
      $(this).parent().toggleClass('open');
    });
    $('body').on('click', function (e) {
      if (!$('.page-header .ph-navbar').is(e.target)
        && $('.page-header .ph-navbar').has(e.target).length === 0
        && $('.page-header .ph-navbar .open').has(e.target).length === 0
      ) {
        $('.page-header .ph-navbar .dropdown.show-more').removeClass('open');
        $('.page-header .ph-navbar .dropdown-l1').removeClass('open');
      }
    });
  }

  //Main slider
  function sliderMain() {
    $('.main-slider .slick-slider').each(function () {
      $(this).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        dots: true,
        appendDots: $('.ms-controls .slick-switches .ss-dots', $(this).closest('.main-slider')),
      });

      $('.ms-controls .slick-switches .ss-switch.prev', $(this).closest(".main-slider")).click(function () {
        $('.slick-slider', $(this).closest('.main-slider')).slick('slickPrev');
      });

      $('.ms-controls .slick-switches .ss-switch.next', $(this).closest(".main-slider")).click(function () {
        $('.slick-slider', $(this).closest('.main-slider')).slick('slickNext');
      });

      var numItems = $('.ms-slide', $(this)).length;
      if (numItems > 1) {
        $('.ms-controls', $(this).closest(".main-slider")).css('display', '');
      }
    })
  };

  // sliderProgress
  function sliderProgress() {
    $('.progress-video .pr-slider .slick-slider-alt').each(function () {
      $(this).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        dots: true,
        centerMode: true,
        focusOnSelect: true,
        variableWidth: true,
        appendDots: $('.pr-controls .slick-switches .ss-dots', $(this).closest('.pr-slider')),

      });

      $('.pr-controls .slick-switches .ss-switch.prev', $(this).closest(".pr-slider")).click(function () {
        $('.slick-slider-alt', $(this).closest('.pr-slider')).slick('slickPrev');
      });

      $('.pr-controls .slick-switches .ss-switch.next', $(this).closest(".pr-slider")).click(function () {
        $('.slick-slider-alt', $(this).closest('.pr-slider')).slick('slickNext');
      });

      var numItems = $('.ss-item', $(this)).length;
      if (numItems > 3 && window.innerWidth >= 992 || numItems > 2 && (window.innerWidth <= 991 && window.innerWidth >= 576) || numItems > 1 && window.innerWidth <= 575) {
        $('.pr-controls', $(this).closest('.pr-slider')).css('display', '');
        $('.pr-slider .slick-slider-alt', $(this).closest('.pr-slider')).removeClass('slick-static');
      }

      var currentSlider = $(this);

      $(window).on('resize', function () {
        waitForFinalEvent(function () {
          var numItems = $('.ss-item', currentSlider).length;
          if (numItems > 3 && window.innerWidth >= 992 || numItems > 2 && (window.innerWidth <= 991 && window.innerWidth >= 576) || numItems > 1 && window.innerWidth <= 575) {
            $('.pr-controls', currentSlider.closest('.pr-slider')).css('display', '');
            $('.pr-slider .slick-slider-alt', currentSlider.closest('.pr-slider')).removeClass('slick-static');
          } else {
            $('.pr-controls', currentSlider.closest('.pr-slider')).css('display', 'none');
            $('.pr-slider .slick-slider-alt', currentSlider.closest('.pr-slider')).addClass('slick-static');
          };
        }, 100, "pr-slider ");
      });
    });
  };

  // Object fill polyfill
  function objectFill() {
    objectFitImages(null, { watchMQ: true });
  }

  //добавление класса к body, для отслеживания ширины экрана
  function customResize() {
    if ($(window).width() < 1200) {
      $(".home").removeClass("display");
      $(".home").addClass("mobile");
    } else {
      $(".home").addClass("display");
      $(".home").removeClass("mobile");
    }
  }
  $(window).resize(customResize);

  // вызов события resize после загрузки страницы
  $(document).ready(function () {
    customResize();
  });

  //закрытие меню при клике вне области
  $(document).click(function (e) {
    var menu = $("#main-navbar-block");
    var menuBtn = $(".navbar-toggle");
    var head = $(".page-header ");
    if (!menuBtn.is(e.target) && !menu.is(e.target) && menu.has(e.target).length === 0 && head.has(e.target).length === 0) {
      menu.removeClass('in');
      $(".page-header").addClass("ph-white");

    };
    if (!e.target.closest('.page-header')) {
      $(".page-header").removeClass("ph-white");
      menuBtn.addClass('collapsed');
    }
  });

  $(document).on('click', '.navbar-toggle', function (e) {
    if (this.classList.contains('collapsed')) {
      setTimeout(() => {
        $(".page-header").removeClass("ph-white");
      });
    }
  })

  function headbtn() {
    $(".page-header").addClass("ph-white");
  }

  //menu scroll
  function menuScroll() {
    if (!$(".main-nav-toggle").hasClass('collapsed') && ($(window).width() < 1199)) {
      $(".navbar-collapse").removeClass('in');
      $(".navbar-toggle").addClass('collapsed');
      $(".page-header").removeClass("ph-white")
    }
  }

  //fixed header
  function fixedHeader() {
    if ($(window).scrollTop() > 22) {
      $('.page-header').addClass('fixed-header');
    }
    else {
      $('.page-header').removeClass('fixed-header');
    }
  }

  //parallax
  function parallax() {

    $('.parallax').each(function (index, item) {
      if ($(item).find('.parallax-bg').length) {
        new Parallax(document.getElementById($(item).find('.parallax-bg').attr('id')), {
          selector: ".parallax-item"
        });
      }
    });
  };


  $(document).ready(function () {
    fixedHeader();
    $(window).scroll(function () {
      fixedHeader();
    });
    footer();
    footerAuto();
    mask();
    mainNavbar();
    navbarCollapse();
    pageHeaderNavbar();
    sliderMain();
    sliderProgress();
    objectFill();
    parallax();
    $(".nav__link").click(menuScroll);
    $(".navbar-toggle").click(headbtn);
  });


})(jQuery);
