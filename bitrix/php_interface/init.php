<?
/*
You can place here your functions and event handlers

AddEventHandler("module", "EventName", "FunctionName");
function FunctionName(params)
{
	//code
}
*/


use Bitrix\Main\Mail\Event;
use Bitrix\Main\Application;

function pr($el){
	echo "<pre>";
	print_r($el);
	echo "</pre>";
}

function pr2($el){
	echo "<!--<pre>";
	print_r($el);
	echo "</pre>-->";
}

AddEventHandler("main", "OnPageStart", "checkSubmitForm");


function checkSubmitForm(){
	$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$post_data = $request->getPostList()->toArray();
		// pr($post_data);
	if($post_data['send-form'] == '1'){
		foreach (['name','phone','message'] as $key => $value) {
			if($post_data[$value] == '')
				return false;
		}
		cevent::sendimmediate('REDPROMO_FORM', 's1', [
			'NAME' => $post_data['name'],  
			'PHONE' => $post_data['phone'],  
			'MESSAGE' => $post_data['message'],  
		]);
		LocalRedirect('/success.php');
	}
}
// checkSubmitForm();



AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "FunctionName");
function FunctionName($newFields)
{
	$res = CIBlockElement::GetByID($newFields['ID']);
	$obRes = $res->GetNextElement();
	if($obRes){
	  $ar_res = $obRes->GetFields();
	}
	// file_put_contents(__DIR__.'/filename.log', var_export($newFields, true) . PHP_EOL . '===================================' .var_export( $ar_res, true));
	if($newFields['ACTIVE'] == 'N' && $ar_res['SHOW_COUNTER'] > 2){
	
           global $APPLICATION;
            $aMsg = array();
            $aMsg[] = array("id"=>"ACTIVE", "text"=>"Товар невозможно деактивировать, у него ${ar_res['SHOW_COUNTER']} просмотров», где ${ar_res['SHOW_COUNTER']} – количество просмотров элемента.");
            $e = new CAdminException($aMsg);
            $APPLICATION->throwException($e);
            return false;

	}

}


AddEventHandler("main", "OnBeforeUserUpdate", "FunctionName2");
AddEventHandler("main", "OnBeforeUserAdd", "FunctionName2");
function FunctionName2($arParams)
{
	if(in_array('12', array_column($arParams['GROUP_ID'], 'GROUP_ID'))){
		$filter = Array
		(
		    "GROUPS_ID"           => Array(12)
		);
		$emails = '';
		$rsUsers = CUser::GetList(($by=""), ($order="desc"), $filter); // выбираем пользователей
		while ($ob = $rsUsers->GetNext()) {
			$emails .= $ob['EMAIL'].',';
		}
		global $APPLICATION;
		$arEventFields['EMAILS'] = $emails;
		$arEventFields['LOGIN'] = $arParams['LOGIN'];
		$arEventFields['EMAIL'] = $arParams['EMAIL'];
		$arEventFields['NAME'] = $arParams['NAME'] != '' ? $arParams['NAME']: '1';
		$arEventFields['LAST_NAME'] = $arParams['LAST_NAME'] != '' ? $arParams['LAST_NAME'] : '1';
		// $event = new CEvent;

		cevent::sendimmediate('CONTENT_EDITOR', SITE_ID, $arEventFields);

		/*$res = Event::send(array(
		    "EVENT_NAME" => "CONTENT_EDITOR",
		    "LID" => SITE_ID,
		    "C_FIELDS" => $arEventFields
		)); */

	    // $res = $event->SendImmediate("CONTENT_EDITOR", SITE_ID, $arEventFields);
	      if($ex = $APPLICATION->GetException()){
		  	$err =  $ex->GetString();
		  }
	}
}


AddEventHandler('main', 'OnBeforeEventSend', Array("MyForm", "my_OnBeforeEventSend"));
class MyForm
{
   function my_OnBeforeEventSend(&$arFields, $arTemplate)
   {
			global $USER;
			if($USER->IsAuthorized()){
				$user_f = cuser::getbyid($USER->getid())->getnext();
				$arFields['AUTHOR'] = "Пользователь авторизован: ${user_f['ID']} ${user_f['LOGIN']} ${user_f['NAME']}, данные из формы: ${arFields['AUTHOR']}";
			}else{
				$arFields['AUTHOR'] = "Пользователь не авторизован, данные из формы: ${arFields['AUTHOR']}";
			}

				CEventLog::Add(array(
	         "SEVERITY" => "SECURITY",
	         "AUDIT_TYPE_ID" => "MY_OWN_TYPE",
	         "MODULE_ID" => "main",
	         "ITEM_ID" => 123,
	         "DESCRIPTION" => "Замена данных в отсылаемом письме – ${arFields['AUTHOR']}",
	      ));
   }
}